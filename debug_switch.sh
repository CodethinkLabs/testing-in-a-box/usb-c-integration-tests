#!/usr/bin/bash
set -euxo pipefail

while [ 1 ] 
do
	echo 'AT+PORT=1' > /dev/ttyACM0
	sleep 1
	counter=0
	while [ ! -e "/dev/sda1" ]
	do
		sleep 0.5
		counter=$(($counter + 1))
		if [[ $counter -gt 20 ]]; then
			exit 1
		fi
	done


	df | grep "/dev/sda1"
	umount /dev/sda1

	echo 'AT+PORT=2' > /dev/ttyACM0
	sleep 1
	counter=0
	while [ ! -e "/dev/sda1" ]
	do
		sleep 0.5
		counter=$(($counter + 1))
		if [[ $counter -gt 20 ]]; then
			exit 1
		fi
	done

	df | grep "/dev/sda1"
	umount /dev/sda1
	echo "Success"
done
