#!/usr/bin/bash
set -euxo pipefail

echo 'AT+PORT=1' > /dev/ttyACM0
sleep 1
counter=0
while [ ! -e "/dev/sda1" ]
do
	sleep 0.5
	counter=$(($counter + 1))
	if [[ $counter -gt 20 ]]; then
		exit 1
	fi
done


df | grep "/dev/sda1"

while [ 1 ] 
do
	rm -f /home/codething/random_to_move
	dd if=/dev/urandom of=/home/codething/random_to_move bs=64M count=16
	mv /home/codething/random_to_move /media/codething/*/
	sync


	echo "Success"
done
