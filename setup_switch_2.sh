#!/usr/bin/bash
set -euxo pipefail

umount /dev/sda1 || true

echo 'AT+PORT=2' > /dev/ttyACM0
sleep 1
counter=0
while [ ! -e "/dev/sda1" ]
do
	sleep 0.5
	counter=$(($counter + 1))
	if [[ $counter -gt 10 ]]; then
		exit 1
	fi
done


df | grep "/dev/sda1"


echo "Success"
