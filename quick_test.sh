#!/usr/bin/bash
set -euxo pipefail

rm -f /home/codething/random_to_move
dd if=/dev/urandom of=/home/codething/random_to_move bs=4M count=4
sha256sum /home/codething/random_to_move
mv /home/codething/random_to_move /media/codething/*/
sync
sleep 1
sha256sum /media/codething/*/random_to_move
mv /media/codething/*/random_to_move /home/codething/moved_back
sync
sleep 1
sha256sum /home/codething/moved_back


#umount /dev/sda1

echo "Success"
