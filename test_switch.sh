#!/usr/bin/bash
set -euxo pipefail

echo 'AT+PORT=1' > /dev/ttyACM0
sleep 1
counter=0
while [ ! -e "/dev/sda1" ]
do
	sleep 0.5
	counter=$(($counter + 1))
	if [[ $counter -gt 10 ]]; then
		exit 1
	fi
done


df | grep "/dev/sda1"

rm -f /home/codething/random_to_move
dd if=/dev/urandom of=/home/codething/random_to_move bs=50k count=1
sha256sum /home/codething/random_to_move
mv /home/codething/random_to_move /media/codething/*/
sync
sleep 1
sha256sum /media/codething/*/random_to_move
mv /media/codething/*/random_to_move /home/codething/moved_back
sync
sleep 1
sha256sum /home/codething/moved_back

umount /dev/sda1


echo 'AT+PORT=2' > /dev/ttyACM0
sleep 1
counter=0
while [ ! -e "/dev/sda1" ]
do
	sleep 0.5
	counter=$(($counter + 1))
	if [[ $counter -gt 10 ]]; then
		exit 1
	fi
done

df | grep "/dev/sda1"

rm -f /home/codething/random_to_move
dd if=/dev/urandom of=/home/codething/random_to_move bs=50k count=1
sha256sum /home/codething/random_to_move
mv /home/codething/random_to_move /media/codething/*/
sync
sleep 1
sha256sum /media/codething/*/random_to_move
mv /media/codething/*/random_to_move /home/codething/moved_back
sync
sleep 1
sha256sum /home/codething/moved_back

umount /dev/sda1
echo "Success"
